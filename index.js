





fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => console.log(data));


/*
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    const titleCollection = data.map((x) => {
      return x.title;
    });
    console.log(titleCollection);
  })
  */



fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(data => {
    const titleCollection = data.map(x => x.title);
    console.log(titleCollection);
  });


  fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => console.log(data))
 


fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => {

    console.log(`The item ${data.title} on the list has a status of ${data.status}`)
  })


  fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: "Created to do list Item",
    id: 201,
    completed: "false",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    id: 1,
    title: "Updated to do list item",
    description: "to update my to do list with a different data structure",
    status: "pending",
    dateCompleted: "pending",
    userId:1
  
  })
})
.then(response => response.json())
.then(json => console.log(json))



fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    completed: false,
    dateCompleted: "07/09/2021",
    id:1,
    status: "complete",
    title: "delectus aut autem",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
})

 //need screenshot